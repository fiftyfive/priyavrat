package com.nibe.weather_assignment;

import android.app.Application;
import android.content.Context;

/**
 * (c) NIBE Industrier, All rights reserved.
 *
 * Application class instance which may need to provide or cache values.
 */
public class WeatherApplication extends Application {

    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }

    public static Context getAppContext() {
        return appContext;
    }
}
