
package com.nibe.weather_assignment.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * (c) NIBE Industrier, All rights reserved.
 *
 * Main model class to hold the complete remote API response.
 */
public class WeatherForecastDetails {

    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("timezone")
    @Expose
    private String timezone;
    @SerializedName("currently")
    @Expose
    private CurrentlyDetails currently;
    @SerializedName("minutely")
    @Expose
    private MinutelyDetails minutely;
    @SerializedName("hourly")
    @Expose
    private HourlyDetails hourly;
    @SerializedName("daily")
    @Expose
    private DailyDetails daily;
    @SerializedName("flags")
    @Expose
    private Flags flags;
    @SerializedName("offset")
    @Expose
    private Integer offset;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public CurrentlyDetails getCurrently() {
        return currently;
    }

    public void setCurrently(CurrentlyDetails currently) {
        this.currently = currently;
    }

    public MinutelyDetails getMinutely() {
        return minutely;
    }

    public void setMinutely(MinutelyDetails minutely) {
        this.minutely = minutely;
    }

    public HourlyDetails getHourly() {
        return hourly;
    }

    public void setHourly(HourlyDetails hourly) {
        this.hourly = hourly;
    }

    public DailyDetails getDaily() {
        return daily;
    }

    public void setDaily(DailyDetails daily) {
        this.daily = daily;
    }

    public Flags getFlags() {
        return flags;
    }

    public void setFlags(Flags flags) {
        this.flags = flags;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

}
