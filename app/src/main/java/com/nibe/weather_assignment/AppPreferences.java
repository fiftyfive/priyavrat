package com.nibe.weather_assignment;

import android.app.Activity;
import android.content.SharedPreferences;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * Singleton class which will take care of all the operations made for shared preferences of the application.
 */
public class AppPreferences {

    private static AppPreferences mAppPreferences = null;
    private static final String PREF_NAME = "nibe_weather_prefs";
    private volatile SharedPreferences mSharedPreferences;
    // Required private constructor to prevent the outside instantiation of class.
    private AppPreferences() {
        mSharedPreferences = WeatherApplication.getAppContext().getSharedPreferences(PREF_NAME, Activity.MODE_PRIVATE);
    }

    /**
     * This method will be returning the instance of same class.
     * By using synchronized, making sure the thread safety here.
     * @return mAppPreferences
     */
    public static AppPreferences getInstance() {
        if (null == mAppPreferences) {
            synchronized (AppPreferences.class) {
                if (null == mAppPreferences) {
                    mAppPreferences = new AppPreferences();
                }
            }
        }
        return mAppPreferences;
    }


    public void putDataIntoPreferences(final String key, final String value) {
        if (null != mSharedPreferences) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            if (null != editor) {
                editor.putString(key, value);
                editor.apply();
            }
        }
    }

    /**
     * This method will be providing the string value stored for a given key.
     * @param key for which the value needs to be retrieved.
     * @return retrieved string value.
     */
    public String getDataFromPreferences(final String key) {
        if (null != mSharedPreferences) {
            String data = mSharedPreferences.getString(key, null);
            return data;
        }
        return null;
    }

    /**
     * Method to store boolean data into preferences.
     * @param key for which the provided value will be stored as key-value pair.
     * @param value true/ false
     */
    public void putBooleanIntoPreferences(final String key, final boolean value) {
        if (null != mSharedPreferences) {
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            if (null != editor) {
                editor.putBoolean(key, value);
                editor.apply();
            }
        }
    }

    /**
     * For extracting boolean data from preferences
     * @param key for mapping the boolean data.
     * @return true/ false
     */
    public boolean getBooleanFromPreferences(final String key) {
        if (null != mSharedPreferences) {
            boolean data = mSharedPreferences.getBoolean(key, false);
            return data;
        }
        return false;
    }
}
