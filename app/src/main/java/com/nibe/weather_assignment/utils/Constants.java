package com.nibe.weather_assignment.utils;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * This class will be containing all the required constants used throughout the application.
 */

public interface Constants {

    // Remote service URL to fetch the weather forecasting data.
    String WEATHER_APP_REQUEST_URL = "https://api.darksky.net/";

    // API Secret, which is needed when making the REST request for retrieving weather data.
    String WEATHER_API_SECRET_KEY = "c3990dcc69ee5cea7303b9259e0a7f39";

    String CURRENT_FORECAST_URL = "forecast";

    // Various forecast types.
    // Those can be passed on(as request parameters) to filter the forecasting results.
    String DAILY = "daily";
    String CURRENTLY = "currently";
    String MINUTELY = "minutely";
    String HOURLY = "hourly";
    String EXCLUDE = "exclude";

    // HTTP response codes. Currently using a few, based on handling more can be added.
    int RESPONSE_CODE_200 = 200;
    int RESPONSE_CODE_204 = 204;
    int RESPONSE_CODE_401 = 401;
    int RESPONSE_CODE_404 = 404;

    // Internal response codes, to identify if there is any internal failure.
    int CODE_UNKNOWN = -1;
    int CODE_LOC_NOT_FOUND = 1000;
    int CODE_CITY_IS_MISSING = 1001;
    int CODE_INTERNET_UNAVAILABLE = 1002;
    int CODE_RESPONSE_NOT_PARSED = 1003;
    int CODE_DB_INSERT_FAILURE = 1004;

    // Keys which are used to persist the data into shared preferences(local secure file storage).
    String CURRENT_LATITUDE = "currentLat";
    String CURRENT_LONGITUDE = "currentLong";
    String CURRENT_CITY_OR_PLACE = "currentCityOrPlace";

    // Messages to be printed.
    String FAILURE_MSG_LOCATION_MISSING = "User's Location not found. Please select your location again.";

    // Declaring tags for various fragments used in the application.
    String FRAG_LOC_INPUT = "userLocFragment";

    // Used charset while parsing the JSON input data.
    String UTF_8 = "UTF_8";
}
