package com.nibe.weather_assignment.utils;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * (c) NIBE Industrier. All rights reserved.
 *
 *  This class will hold the utility methods required frequently for the operations in app.
 */
public final class AppUtils {

    public static void addFragment(final AppCompatActivity activity,
                                   final @IdRes int containerViewId,
                                   final @NonNull Fragment fragment,
                                   final @NonNull String fragmentTag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    public static void replaceFragment(final AppCompatActivity activity,
                                       final @IdRes int containerViewId,
                                       final @NonNull Fragment fragment,
                                       final @NonNull String fragmentTag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit();
    }

    /**
     * Utility method to navigate to desired target activity.
     * @param context needed to start the intent.
     * @param targetClass the activity which needs to be started.
     * @param extrasTag for which the extra information needs to be shared.
     * @param extras data to be propagated to target activity.
     */
    public static void navigateToAnotherActivity(final Context context,
                                                 @NonNull final Class targetClass,
                                                 final String extrasTag,
                                                 final Bundle extras) {
        Intent navigationIntent = new Intent(context, targetClass);
        if (null != extras && null != extrasTag && !extrasTag.trim().isEmpty()) {
            navigationIntent.putExtra(extrasTag, extras);
        }
        context.startActivity(navigationIntent);
    }

    /**
     * Method to check if the active internet connection is available or not.
     * @param context to get system service instance.
     * @return true/ false based on the active internet status.
     */
    public static boolean isConnectedToInternet(final Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
