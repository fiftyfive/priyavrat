package com.nibe.weather_assignment.utils;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * (c) 2019 NIBE Inc. All rights reserved.
 *
 * Type converter to convert string arraylist into comma separated array and arraylist from comma separated string.
 * Such operations are required when try to put array into database, as database doesn't support the array.
 */

public class ArrayListToStringTypeConverter {

    @TypeConverter
    public static ArrayList<String> stringToSomeObjectList(String data) {
        if (data == null) {
            return null;
        }

        Type listType = new TypeToken<ArrayList<Object>>() {}.getType();

        return new Gson().fromJson(data, listType);
    }

    @TypeConverter
    public static String someObjectListToString(ArrayList<String> someObjects) {
        return new Gson().toJson(someObjects);
    }
}
