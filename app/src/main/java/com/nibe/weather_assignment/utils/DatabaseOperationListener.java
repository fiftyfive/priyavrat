package com.nibe.weather_assignment.utils;

/**
 * (c) NIBE Industrier. All rights reserved.
 *
 * Callback, will get notified when there is update on database operations.
 */
public interface DatabaseOperationListener {

    void onOperationCompleted(boolean isSuccess, Object response);
}
