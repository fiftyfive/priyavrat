package com.nibe.weather_assignment.utils;

import android.view.View;

/**
 * (c) NIBE Industrier. All rights reserved.
 *
 * Common listeners used for recycler view item click, any sub view click etc.
 */
public interface RecyclerViewListeners {

    interface ListItemClickListener {

        void onItemClick(View clickedView, Object clickedItemData, int clickedPosition);
    }

    interface SubViewClickListener {
            void onSubViewClicked(int clickedPosition, Object clickedItemData);
    }
}
