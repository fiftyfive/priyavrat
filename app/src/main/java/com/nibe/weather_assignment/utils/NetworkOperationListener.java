package com.nibe.weather_assignment.utils;

/**
 * (c) NIBE Industrier. All rights reserved.
 *
 * Callback, will get notified when there is update from network API.
 * Either it is success response or failure response.
 */
public interface NetworkOperationListener <T> {

    void onOperationStarted();
    void onOperationCompleted(int responseCode, T response);
    void onOperationFailure(int failureCode, T failureMessage);
}
