package com.nibe.weather_assignment.viewmodel;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * A communication layer between view and view model, this will get notified when operation is done in view model-> model
 * which was requested from view.
 */
public interface DataChangeListener {

    void onUpdated(boolean isSuccess, Object response);
}
