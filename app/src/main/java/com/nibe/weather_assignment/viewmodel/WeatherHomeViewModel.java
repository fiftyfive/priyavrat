package com.nibe.weather_assignment.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nibe.weather_assignment.database.entities.CurrentlyDataEntity;
import com.nibe.weather_assignment.database.entities.HourlyDataEntity;
import com.nibe.weather_assignment.database.entities.WeatherForecastEntity;
import com.nibe.weather_assignment.models.CurrentlyDetails;
import com.nibe.weather_assignment.models.HourlyData;
import com.nibe.weather_assignment.models.HourlyDetails;
import com.nibe.weather_assignment.models.WeatherForecastDetails;
import com.nibe.weather_assignment.AppPreferences;
import com.nibe.weather_assignment.utils.Constants;
import com.nibe.weather_assignment.utils.DatabaseOperationListener;
import com.nibe.weather_assignment.utils.NetworkOperationListener;

import java.util.ArrayList;
import java.util.List;

import static com.nibe.weather_assignment.utils.Constants.CODE_LOC_NOT_FOUND;
import static com.nibe.weather_assignment.utils.Constants.CURRENT_CITY_OR_PLACE;
import static com.nibe.weather_assignment.utils.Constants.CURRENT_LATITUDE;
import static com.nibe.weather_assignment.utils.Constants.CURRENT_LONGITUDE;
import static com.nibe.weather_assignment.utils.Constants.FAILURE_MSG_LOCATION_MISSING;
import static com.nibe.weather_assignment.utils.Constants.RESPONSE_CODE_200;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * This view model will take care of below operations.
 * - Fetch the record from remote.
 * - Fetch the record from local.
 * - Insert the record into local when loaded from remote.
 */
public class WeatherHomeViewModel extends BaseViewModel {

    private static final String TAG = WeatherHomeViewModel.class.getSimpleName();

    public WeatherHomeViewModel(@NonNull Application application) {
        super(application);
    }

    public void storeGeoCoordinatesAndPlace(final double latitude,
                                            final double longitude,
                                            @NonNull final String place) {

        AppPreferences appPreferences = AppPreferences.getInstance();

        appPreferences.putDataIntoPreferences(CURRENT_CITY_OR_PLACE, place);
        appPreferences.putDataIntoPreferences(CURRENT_LATITUDE, String.valueOf(latitude));
        appPreferences.putDataIntoPreferences(CURRENT_LONGITUDE, String.valueOf(longitude));
    }
    /**
     * Method to fetch the data from remote either on-demand(When user want to refresh it.) or
     * if the data is not found in the local storage.
     */
    public void loadWeatherDataFromRemote(final NetworkOperationListener<Object> networkOperationListener) {

        // First fetching the current stored latitude and longitude from local storage(Shared Preferences).
        AppPreferences appSavedPref = AppPreferences.getInstance();

        String currentLatitude = appSavedPref.getDataFromPreferences(CURRENT_LATITUDE);
        String currentLongitude = appSavedPref.getDataFromPreferences(CURRENT_LONGITUDE);

        // If the user's latitude and longitude not found then prompt for missing & request to ask the location again.
        if (null == currentLatitude || currentLatitude.trim().isEmpty()
                || null == currentLongitude || currentLongitude.trim().isEmpty()) {
            Log.e(TAG, Constants.FAILURE_MSG_LOCATION_MISSING);
            if (null != networkOperationListener) {
                networkOperationListener.onOperationFailure(CODE_LOC_NOT_FOUND, FAILURE_MSG_LOCATION_MISSING);
            }
            return;
        }

        // Initiating the process to fetch the location from remote.
        mNetworkRepository.getCurrentForecastFromServer(currentLatitude, currentLongitude, new NetworkOperationListener() {
            @Override
            public void onOperationStarted() {
                if (null != networkOperationListener) {
                    networkOperationListener.onOperationStarted();
                }
            }

            @Override
            public void onOperationCompleted(int responseCode, Object response) {

                if (null != networkOperationListener) {
                    networkOperationListener.onOperationCompleted(responseCode, response);
                }

                // Check if proper response is received from server.
                if (responseCode == RESPONSE_CODE_200) {

                    // If response is available then
                    if (response instanceof WeatherForecastDetails) {
                        WeatherForecastDetails forecastDetails = (WeatherForecastDetails) response;

                        // Proceed to insert the data-set into database.
                        recordWeatherDataIntoStorage(forecastDetails, null);

                    }
                }
            }

            @Override
            public void onOperationFailure(int code, Object failureMessage) {
                if (null != networkOperationListener) {
                    networkOperationListener.onOperationFailure(code, failureMessage);
                }
            }
        });
    }

    /**
     * API which will be called initially, will return the weather record if exist in database.
     * If not exist then it will initiate to the fetch the location from remote.
     * @param dataChangeListener notify that
     */
    public void loadWeatherDataFromLocal (final DataChangeListener dataChangeListener) {

        // Load the current city from shared preferences.
        AppPreferences appSavedPref = AppPreferences.getInstance();
        String placeOrCity = appSavedPref.getDataFromPreferences(CURRENT_CITY_OR_PLACE);

        mDatabaseRepository.getWeatherForecastDataForCurrentLocation(placeOrCity, new DatabaseOperationListener() {
            @Override
            public void onOperationCompleted(boolean isSuccess, Object response) {

                // If the record not found in database then proceed to fetch that from remote.
                if (isSuccess && null == response) {

                    // No need to provide the listener here, because when the data is loaded from remote,
                    // It will be inserted into DB and will quickly reflect as LiveData is used when fetching the records,
                    // Which have an observer mechanism for same.
                    loadWeatherDataFromRemote(null);
                }

                // Sending the callback to the caller.
                if (null != dataChangeListener) {
                    dataChangeListener.onUpdated(isSuccess, response);
                }
            }
        });
    }

    /**
     * This method will be initiating the process to insert the weather information into database.
     * @param weatherForecastDetails object which needs to be recorded.
     * @param dataChangeListener tell about the operation result to caller.
     */
    private void recordWeatherDataIntoStorage (@NonNull final WeatherForecastDetails weatherForecastDetails,
                                               final DataChangeListener dataChangeListener) {

        if (null == weatherForecastDetails) {
            Log.w(TAG, "Remote weather response is empty, Can't flush the data into DB.");
            return;
        }
        // Preparing the insertion object from the data received from remote.
        WeatherForecastEntity weatherForecastEntity = filterWeatherDataToPersist(weatherForecastDetails);

        mDatabaseRepository.insertWeatherForecastDataIntoDB(weatherForecastEntity, new DatabaseOperationListener() {
            @Override
            public void onOperationCompleted(boolean isSuccess, Object response) {
                if (null != dataChangeListener) {
                    dataChangeListener.onUpdated(isSuccess, response);
                }
            }
        });
    }

    /**
     * This method is preparing Entity object which needs to be inserted into database.
     * Although the Model WeatherForecastEntity is subset of WeatherForecastDetails.
     * It is not recommended to use the same model for database entity and handling network response.
     * So we are using a separate model instead.
     * @param weatherForecastDetails product which needs to be inserted to database.
     * @return entity data which was formed with the provided model received from remote.
     */
    private WeatherForecastEntity filterWeatherDataToPersist (@NonNull final WeatherForecastDetails weatherForecastDetails) {

        WeatherForecastEntity weatherForecastEntity = new WeatherForecastEntity();

        // Creating the current data entity with the response loaded from remote.
        CurrentlyDetails currentlyDetails = weatherForecastDetails.getCurrently();
        if (null != currentlyDetails) {
            CurrentlyDataEntity preparedCurrentDataEntity = prepareCurrentlyWeatherDataToPersist(currentlyDetails);
            weatherForecastEntity.setCurrentlyDataEntity(preparedCurrentDataEntity);
        }

        // Preparing the hourly data entity with the response loaded from remote.
        HourlyDetails hourlyDetails = weatherForecastDetails.getHourly();
        if (null != hourlyDetails) {
            List<HourlyDataEntity> hourlyDataEntityList = prepareHourlyWeatherDataToPersist(hourlyDetails);
            weatherForecastEntity.setHourlyDataEntity(hourlyDataEntityList);
        }
        return weatherForecastEntity;
    }

    /**
     * Filtering out and preparing the hourly forecasting data array as database entity.
     * @param hourlyDetails received from remote server or the weather endpoint.
     * @return hourlyForecastEntityList, which contains all the forecast data needs to be flushed into local storage.
     */
    private List<HourlyDataEntity> prepareHourlyWeatherDataToPersist(final HourlyDetails hourlyDetails) {

        if (null == hourlyDetails) {
            Log.w(TAG, "Remote hourly forecasting response is empty, Can't prepare the hourly data flush.");
            return null;
        }

        List<HourlyData> hourlyForecastList = hourlyDetails.getData();
        List<HourlyDataEntity> hourlyForecastEntityList = new ArrayList<>();

        for (HourlyData hourlyData : hourlyForecastList) {

            HourlyDataEntity hourlyDataEntity = new HourlyDataEntity();

            hourlyDataEntity.setTime(hourlyData.getTime());
            hourlyDataEntity.setTemperature(hourlyData.getTemperature());
            hourlyDataEntity.setApparentTemperature(hourlyData.getApparentTemperature());
            hourlyDataEntity.setCloudCover(hourlyData.getCloudCover());
            hourlyDataEntity.setWindSpeed(hourlyData.getWindSpeed());
            hourlyDataEntity.setHumidity(hourlyData.getHumidity());
            hourlyDataEntity.setSummary(hourlyData.getSummary());
            hourlyDataEntity.setVisibility(hourlyData.getVisibility());
            hourlyDataEntity.setPressure(hourlyData.getPressure());
            hourlyForecastEntityList.add(hourlyDataEntity);
        }
        return hourlyForecastEntityList;
    }

    /**
     * Filtering out and preparing the current forecasting data.
     * @param currentlyDetails received current forecasting details from weather endpoint.
     * @return currentlyDataEntity, currently weather records which need to be flushed into local storage.
     */
    private CurrentlyDataEntity prepareCurrentlyWeatherDataToPersist (CurrentlyDetails currentlyDetails) {

        if (null == currentlyDetails) {
            Log.w(TAG, "Remote currently forecasting response is empty, Can't prepare the currently data flush.");
            return null;
        }

        CurrentlyDataEntity currentlyDataEntity = new CurrentlyDataEntity();
        currentlyDataEntity.setTime(currentlyDetails.getTime());
        currentlyDataEntity.setSummary(currentlyDetails.getSummary());
        currentlyDataEntity.setIcon(currentlyDetails.getIcon());
        currentlyDataEntity.setNearestStormDistance(currentlyDetails.getNearestStormDistance());
        currentlyDataEntity.setNearestStormBearing(currentlyDetails.getNearestStormBearing());
        currentlyDataEntity.setPrecipIntensity(currentlyDetails.getPrecipIntensity());
        currentlyDataEntity.setPrecipProbability(currentlyDetails.getPrecipProbability());
        currentlyDataEntity.setTemperature(currentlyDetails.getTemperature());
        currentlyDataEntity.setHumidity(currentlyDetails.getHumidity());
        currentlyDataEntity.setPressure(currentlyDetails.getPressure());
        currentlyDataEntity.setWindSpeed(currentlyDetails.getWindSpeed());
        currentlyDataEntity.setVisibility(currentlyDetails.getVisibility());
        currentlyDataEntity.setCloudCover(currentlyDetails.getCloudCover());
        currentlyDataEntity.setOzone(currentlyDetails.getOzone());

        return currentlyDataEntity;
    }
}
