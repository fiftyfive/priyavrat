package com.nibe.weather_assignment.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.nibe.weather_assignment.database.WeatherForecastDatabaseRepository;
import com.nibe.weather_assignment.network.WeatherForecastNetworkRepository;

/**
 * (c) NIBE Industrier, All rights reserved.
 *
 * Base View model will hold all common objects required for each view model functionality etc.
 */
public abstract class BaseViewModel extends AndroidViewModel {

    final WeatherForecastDatabaseRepository mDatabaseRepository;
    final WeatherForecastNetworkRepository mNetworkRepository;

    /**
     * Public constructor, initiating the required components like database repository.
     * @param application current application context.
     */
    public BaseViewModel(@NonNull Application application) {
        super(application);
        mDatabaseRepository = WeatherForecastDatabaseRepository.getInstance(application);
        mNetworkRepository = WeatherForecastNetworkRepository.getInstance();
    }
}
