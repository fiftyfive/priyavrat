package com.nibe.weather_assignment.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.nibe.weather_assignment.database.dao.WeatherAppForecastDao;
import com.nibe.weather_assignment.database.entities.WeatherForecastEntity;

/**
 * * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * Creating the database instance at once and reusing that throughout the application.
 */
@Database(entities = {WeatherForecastEntity.class}, version = 1)
public abstract class DatabaseHelper extends RoomDatabase {

    public abstract WeatherAppForecastDao weatherForecastDao();

    // Database name.
    private static final String DATABASE_NAME = "weather_app_storage";

    private static volatile DatabaseHelper mSharedDatabaseInstance;

    static DatabaseHelper getDatabase(final Context context) {

        // Making sure that the instance will only be created at once for the application.
        if (null == mSharedDatabaseInstance) {
            synchronized (DatabaseHelper.class) {
                if (null == mSharedDatabaseInstance) {
                    mSharedDatabaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                            DatabaseHelper.class, DATABASE_NAME)
                            .build();
                }
            }
        }
        return mSharedDatabaseInstance;
    }
}
