package com.nibe.weather_assignment.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.nibe.weather_assignment.database.dao.WeatherAppForecastDao;
import com.nibe.weather_assignment.database.entities.WeatherForecastEntity;
import com.nibe.weather_assignment.utils.DatabaseOperationListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * * (c) 2019 NIBE Industrier . All rights reserved.
 *
 * This is the separation layer of database operations and rest of the app components.
 * Asynchronously it will make the database calls and will provide the results to the callers.
 * Keeping it as singleton, as creating many object of this may lead data inconsistency and extra overhead to system.
 **/
public class WeatherForecastDatabaseRepository {

    private final WeatherAppForecastDao mWeatherForecastDao;

    private LiveData<WeatherForecastEntity> mCurrentWeatherRecord;

    // Sequential(Single threaded) worker executor, To make sure that database operations are not main thread.
    private ExecutorService mDatabaseWorkerExecutor = Executors.newSingleThreadExecutor();

    // Keeping volatile static reference will help to prevent the duplicate instantiation, as it guarantees for most updated value.
    private static volatile WeatherForecastDatabaseRepository mRepoShareInstance;

    private WeatherForecastDatabaseRepository(final Application application) {
        DatabaseHelper database = DatabaseHelper.getDatabase(application);
        mWeatherForecastDao = database.weatherForecastDao();
    }

    public static WeatherForecastDatabaseRepository getInstance(final Application application) {
        if (null == mRepoShareInstance) {
            synchronized (WeatherForecastDatabaseRepository.class) {
                mRepoShareInstance = new WeatherForecastDatabaseRepository(application);
            }
        }
        return mRepoShareInstance;
    }

    /**
     * Returning the complete records of weather forecast table.
     */
    public void getWeatherForecastDataForCurrentLocation(final String placeOrCity, final DatabaseOperationListener databaseOperationListener) {

        if (null == mCurrentWeatherRecord) {

            restartWorkerExecutorIfNeeded();

            mDatabaseWorkerExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    mCurrentWeatherRecord = mWeatherForecastDao.retrieveWeatherRecordForSpecificLocation(placeOrCity);
                    if (null != databaseOperationListener) {
                        databaseOperationListener.onOperationCompleted(null != mCurrentWeatherRecord, mCurrentWeatherRecord);
                    }
                }
            });
        } else {
            // If the current forecasting data is already loaded then no need to make the database operations.
            if (null != databaseOperationListener) {
                databaseOperationListener.onOperationCompleted(true, mCurrentWeatherRecord);
            }
        }
    }

    /**
     * This method will insert the current weather information into database.
     * If the weather forecasting data is already available for the particular city/ place.
     * Then it will be updated by this call.
     * @param weatherForecastEntity Data which needs to be inserted/ updated into database.
     * @param databaseOperationListener
     */
    public void insertWeatherForecastDataIntoDB(@NonNull final WeatherForecastEntity weatherForecastEntity,
                                                final DatabaseOperationListener databaseOperationListener) {

        restartWorkerExecutorIfNeeded();

        mDatabaseWorkerExecutor.execute(new Runnable() {
            @Override
            public void run() {
                long isUpdated = mWeatherForecastDao.storeWeatherForecastData(weatherForecastEntity);
                if (null != databaseOperationListener) {
                    databaseOperationListener.onOperationCompleted(-1 != isUpdated, null);
                }
            }
        });
    }

    /**
     * This method is used to restart the pool executor if it is stopped or set to null.
     */
    private void restartWorkerExecutorIfNeeded () {
        if((null == mDatabaseWorkerExecutor) || (mDatabaseWorkerExecutor.isShutdown())) {
            mDatabaseWorkerExecutor = Executors.newSingleThreadExecutor();
        }
    }
}
