package com.nibe.weather_assignment.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * Main entity class to which holds all the information about weather forecast like temperature, wind speed, precipitation etc.
 * This is also used to retrieve and insert data to database.
 */

@Entity(tableName = "weather_forecast_data")
public class WeatherForecastEntity {

   public WeatherForecastEntity () {}
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "cityId")
    public String cityId;

    @ColumnInfo(name = "timezone")
    private String timezone;

    @Ignore
    private CurrentlyDataEntity currentlyDataEntity;

    @Ignore
    private List<HourlyDataEntity> hourlyDataEntity;

    @NonNull
    public String getCityId() {
        return cityId;
    }

    public void setCityId(@NonNull String cityId) {
        this.cityId = cityId;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public CurrentlyDataEntity getCurrentlyDataEntity() {
        return currentlyDataEntity;
    }

    public void setCurrentlyDataEntity(CurrentlyDataEntity currentlyDataEntity) {
        this.currentlyDataEntity = currentlyDataEntity;
    }

    public List<HourlyDataEntity> getHourlyDataEntity() {
        return hourlyDataEntity;
    }

    public void setHourlyDataEntity(List<HourlyDataEntity> hourlyDataEntity) {
        this.hourlyDataEntity = hourlyDataEntity;
    }
}
