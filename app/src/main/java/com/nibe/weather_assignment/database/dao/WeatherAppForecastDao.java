package com.nibe.weather_assignment.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.nibe.weather_assignment.database.entities.WeatherForecastEntity;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 * Data Access Object class, using the methods in this class, Room will make the queries to the database.
 * Various CRUD operations are defined here.
 */
@Dao
public interface WeatherAppForecastDao {

    /**
     * Insert a weather information data into DB, if that already exists then update it.
     * @param weatherForecastEntity data which needs to be inserted/ updated.
     * @return status that weather the operation is successful or not.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long storeWeatherForecastData(WeatherForecastEntity weatherForecastEntity);

    @Update
    int updateRecordToCart(WeatherForecastEntity productsEntity);

    @Query("DELETE FROM weather_forecast_data WHERE cityId = :placeOrCity")
    int deleteForecastDataFromDB(String placeOrCity);

    /**
     * Returning the weather forecast record based on provided city/place
     * @param placeOrCity This is the primary key to fetch the record.
     * @return specific record which holds the required weather information.
     */
    @Query("SELECT * from weather_forecast_data WHERE cityId = :placeOrCity")
    LiveData<WeatherForecastEntity> retrieveWeatherRecordForSpecificLocation(String placeOrCity);
}
