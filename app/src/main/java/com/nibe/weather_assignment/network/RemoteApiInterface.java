package com.nibe.weather_assignment.network;

import com.nibe.weather_assignment.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;

import static com.nibe.weather_assignment.utils.Constants.WEATHER_API_SECRET_KEY;

/**
 * (c) NIBE Industrier, All rights reserved.
 * Defines all the api calls to fetch the weather details.
 */

public interface RemoteApiInterface {

    @GET(Constants.CURRENT_FORECAST_URL + "/" + WEATHER_API_SECRET_KEY + "/")
    Call<Object> getCurrentWeatherForecast(String latitude, String longitude);

}
