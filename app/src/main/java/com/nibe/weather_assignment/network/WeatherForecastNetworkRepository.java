package com.nibe.weather_assignment.network;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.nibe.weather_assignment.models.WeatherForecastDetails;
import com.nibe.weather_assignment.utils.NetworkOperationListener;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.nibe.weather_assignment.utils.Constants.CODE_RESPONSE_NOT_PARSED;
import static com.nibe.weather_assignment.utils.Constants.CODE_UNKNOWN;

/**
 * (c) 2019 NIBE Industrier, All rights reserved.
 * This class will take care of handling all the network API calls.
 */
public class WeatherForecastNetworkRepository {

    private static volatile WeatherForecastNetworkRepository mWeatherForecastNetworkRepository;

    private RemoteApiInterface mRemoteApiInterface;

    private WeatherForecastNetworkRepository() {
        mRemoteApiInterface = ApiClient.getInstance().getApiInterface();
    }

    public static WeatherForecastNetworkRepository getInstance() {

        if (null == mWeatherForecastNetworkRepository) {
            synchronized (WeatherForecastNetworkRepository.class) {
                if (null == mWeatherForecastNetworkRepository) {
                    mWeatherForecastNetworkRepository = new WeatherForecastNetworkRepository();
                }
            }
        }
        return mWeatherForecastNetworkRepository;
    }

    /**
     * Based on the provided input params, This method will execute remote API call to fetch the weather inforamtion.
     * @param latitude for which the weather information to be fetched.
     * @param longitude for which the weather information to be fetched.
     * @param networkOperationListener will be notified in either success or failure cases.
     */
    public void getCurrentForecastFromServer(@NonNull final String latitude,
                                             @NonNull final String longitude,
                                             final NetworkOperationListener networkOperationListener) {

        // Sending the request initiation callback.
        if (null != networkOperationListener) {
            networkOperationListener.onOperationStarted();
        }

        mRemoteApiInterface.getCurrentWeatherForecast(latitude, longitude).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                try {
                    if (response.isSuccessful()) {
                        ResponseBody data = (ResponseBody) response.body();
                        WeatherForecastDetails forecastDetails = null;
                        String responseString = data.toString();

                        if (null != responseString) {
                            Gson gsonConverter = new Gson();
                            forecastDetails = gsonConverter.fromJson(responseString, WeatherForecastDetails.class);
                        }

                        // Sending the callback after parsing the network response.
                        if (null != networkOperationListener) {
                            networkOperationListener.onOperationCompleted(response.code(), forecastDetails);
                        }

                    } else {
                        ResponseBody error = response.errorBody();

                        // Sending the error response to caller.
                        if (null != networkOperationListener) {
                            networkOperationListener.onOperationFailure(response.code(), error.toString());
                        }
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();

                    // Sending error response if it fails in any case like parsing the information
                    if (null != networkOperationListener) {
                        networkOperationListener.onOperationFailure(CODE_RESPONSE_NOT_PARSED,"Failed to fetch weather information.");
                    }
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                if (null != networkOperationListener) {
                    networkOperationListener.onOperationFailure(CODE_UNKNOWN, t);
                }
            }
        });
    }
}
