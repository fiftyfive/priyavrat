package com.nibe.weather_assignment.views.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nibe.weather_assignment.R;

/**
 * (c) NIBE Industrier, All rights reserved.
 *
 * Fragment class to display the weather forecast information.
 */
public class ForecastDashboardFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_forecast_dashboard, container, false);

        initViews(rootView);

        return rootView;
    }

    @Override
    void initViews(View rootView) {

    }
}
