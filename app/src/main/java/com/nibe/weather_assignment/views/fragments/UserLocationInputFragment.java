package com.nibe.weather_assignment.views.fragments;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.nibe.weather_assignment.R;
import com.nibe.weather_assignment.utils.AppUtils;
import com.nibe.weather_assignment.viewmodel.WeatherHomeViewModel;
import com.nibe.weather_assignment.views.activities.HomeActivity;

import java.util.Arrays;
import java.util.List;

/**
 * (c) 2019, NIBE Industrier, All rights reserved.
 *
 * Fragment class to take location input from user
 */
public class UserLocationInputFragment extends BaseFragment {

    private static final String TAG = UserLocationInputFragment.class.getSimpleName();

    private WeatherHomeViewModel mWeatherHomeViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeatherHomeViewModel = ViewModelProviders.of(this).get(WeatherHomeViewModel.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user_location_input, container, false);

        initViews(rootView);

        return rootView;
    }

    @Override
    void initViews(View rootView) {

        if (!Places.isInitialized()) {
            Places.initialize(getActivity().getApplicationContext(), "AIzaSyChj9Z_HpIb4-vRIYcrUfN0PUPCUwmXlZI");
        }

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
        autocompleteFragment.setPlaceFields(fields);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                // Persisting the geo-graphical location information, If available.
                if (null != place) {
                    LatLng latLng = place.getLatLng();

                    double latitude = latLng.latitude;
                    double longitude = latLng.longitude;
                    String placeName = place.getName().toString();

                    mWeatherHomeViewModel.storeGeoCoordinatesAndPlace(latitude, longitude, placeName);

                    // Move ahead to display the weather forecasting based on the provided location.
                    AppUtils.navigateToAnotherActivity(getActivity(), HomeActivity.class, null, null);
                }
            }
            @Override
            public void onError(Status status) {
                Log.e(TAG, "Unable to find desired location!! " + status.getStatusMessage());
            }
        });
    }
}
