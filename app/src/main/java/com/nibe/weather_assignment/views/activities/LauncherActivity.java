package com.nibe.weather_assignment.views.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

import com.nibe.weather_assignment.AppPreferences;
import com.nibe.weather_assignment.R;
import com.nibe.weather_assignment.utils.AppUtils;
import com.nibe.weather_assignment.views.fragments.UserLocationInputFragment;

import static com.nibe.weather_assignment.utils.Constants.CURRENT_CITY_OR_PLACE;
import static com.nibe.weather_assignment.utils.Constants.CURRENT_LATITUDE;
import static com.nibe.weather_assignment.utils.Constants.CURRENT_LONGITUDE;
import static com.nibe.weather_assignment.utils.Constants.FRAG_LOC_INPUT;

/**
 * (c) NIBE Industrier, All rights reserved.
 *
 * This class will be working as launcher screen, can also work on initialization if needed.
 */
public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        displayUserLocationScreenWithDelayIfRequired();
    }

    private void displayUserLocationScreenWithDelayIfRequired() {
        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                // For First Time or user has not provided his location yet.
                // Then display the location input screen to get user's desired location.
                AppPreferences preferences = AppPreferences.getInstance();

                if (null == preferences.getDataFromPreferences(CURRENT_CITY_OR_PLACE)
                        || null == preferences.getDataFromPreferences(CURRENT_LATITUDE)
                        || null == preferences.getDataFromPreferences(CURRENT_LONGITUDE)) {
                    AppUtils.addFragment(LauncherActivity.this,
                            R.id.frag_container_launcher, new UserLocationInputFragment(), FRAG_LOC_INPUT);
                } else {
                    // If the user has already provided location/ place then move to the weather forecast screen.
                    AppUtils.navigateToAnotherActivity(LauncherActivity.this, HomeActivity.class, null, null);
                }
            }
        }.start();
    }
}
