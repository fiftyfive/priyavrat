package com.nibe.weather_assignment.views.fragments;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * (c) 2019 NIBE Industrier. All rights reserved.
 *
 * Keeping the common functionality for all the fragments classes.
 **/
public abstract class BaseFragment extends Fragment {

    abstract void initViews (View rootView);
}
