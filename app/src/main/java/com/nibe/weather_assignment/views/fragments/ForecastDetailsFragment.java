package com.nibe.weather_assignment.views.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nibe.weather_assignment.R;

/**
 * (c) 2019 NIBE Industrier, All rights reserved.
 *
 * This Fragment will be responsible to present the following UI.
 * - Displaying the graphical representation of hourly change in temperature.
 * - Displaying a list of hourly temperature change.
 */
public class ForecastDetailsFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment.
        View rootView = inflater.inflate(R.layout.fragment_forecast_details, container, false);

        initViews(rootView);

        return rootView;
    }

    @Override
    void initViews(View rootView) {

    }
}
