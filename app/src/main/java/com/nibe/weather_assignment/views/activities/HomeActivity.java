package com.nibe.weather_assignment.views.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nibe.weather_assignment.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
